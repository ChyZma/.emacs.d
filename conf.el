(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (monokai)))
 '(custom-safe-themes
   (quote
    ("95b0bc7b8687101335ebbf770828b641f2befdcf6d3c192243a251ce72ab1692" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#292929" :foreground "#F5F5F5" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "outline" :family "Liberation Mono")))))

(setq-default c-basic-offset 4)

(add-to-list 'default-frame-alist '(fullscreen . maximized))
(global-linum-mode 1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(cua-mode t)
(setq cua-auto-tabify-reinitctangles nil) ;; Don't tabify after rectanglemacs/b  \ \  \ \  \ \  \____\|__in/e commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour
(setq-default cursor-type 'bar)

;; highlight current line
(global-hl-line-mode t)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(global-set-key (kbd "<f6>") (lambda() (interactive)(find-file "~/.emacs.d/conf.org")))
(global-set-key (kbd "<f5>") (lambda() (interactive)(find-file "~/Dropbox/Org/i.org")))

(setq org-log-done 'time)
(setq org-startup-indented t
      org-hide-leading-stars t)
(setq package-check-signature nil)
(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-files
      '("~/Dropbox/Org"))

(global-set-key (kbd "C-c c")
       'org-capture)

(setq org-capture-templates
      '(
    ("a" "Appointment" entry (file+headline  "~/Dropbox/Org/i.org" "Agenda")
	 "* %?\n    \nSCHEDULED: %^T \n %i\n")
	("n" "Note" entry (file+olp "~/Dropbox/Org/i.org" "Notes" "Unsorted Notes")
	 "* Note %?\n")
	("l" "Link" entry (file+headline "~/Dropbox/Org/i.org" "Links")
	 "* %? %^L %^g \n" :prepend t)
	("t" "To Do Item" entry (file+headline "~/Dropbox/Org/i.org" "TO-DO Stuff")
	"* TODO %?\n" :prepend t)
	("j" "Journal" entry (file+datetree "~/Dropbox/Org/journal.org")
	 "* %?\nEntered on %U\n  %i\n  %a")))

(defcustom hl-todo-keyword-faces
  '(("HOLD" . "#d0bf8f")
    ("TODO" . "#ff4500")
    ("NEXT" . "#dca3a3")
    ("THEM" . "#dc8cc3")
    ("PROG" . "#7cb8bb")
    ("OKAY" . "#7cb8bb")
    ("DONT" . "#5f7f5f")
    ("FAIL" . "#8c5353")
    ("DONE" . "#afd8af")
    ("NOTE"   . "#f5f5dc")
    ("KLUDGE" . "#d0bf8f")
    ("HACK"   . "#d0bf8f")
    ("TEMP"   . "#d0bf8f")
    ("FIXME"  . "#0000ff")
    ("XXX"    . "#cc9393")
    ("XXXX"   . "#cc9393")
    ("BUG"   . "#ff0000")
    ("???"    . "#cc9393"))
  "Faces used to highlight specific TODO keywords."
  :package-version '(hl-todo . "2.0.0")
  :group 'hl-todo
  :type '(repeat (cons (string :tag "Keyword")
                       (choice :tag "Face   "
                               (string :tag "Color")
                               (sexp :tag "Face")))))
(global-hl-todo-mode)

(require 'dashboard)
(dashboard-setup-startup-hook)
;; Or if you use use-package
(use-package dashboard
  :config
  (dashboard-setup-startup-hook))
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
;; Set the title
(setq dashboard-banner-logo-title "Mark is Love, Mark is Life")
;; Set the banner
(setq dashboard-startup-banner 'official)

(use-package hungry-delete
:ensure t
:config
(global-hungry-delete-mode))

(global-set-key [f8] 'neotree-toggle)

(powerline-default-theme)
(show-paren-mode 1)
(setq tramp-default-method "plink")



(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(use-package swiper
  :ensure t
  :config
  (progn 
    (setq enable-recursive-minibuffers t)
    (global-set-key "\C-s" 'swiper)
    ))

(use-package undo-tree
  :ensure t
  :init
  (global-undo-tree-mode)
  (define-key undo-tree-map (kbd "C-y") 'undo-tree-redo)
  (define-key undo-tree-map (kbd "C-z") 'undo-tree-undo)
  )

(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
